<?php

namespace Drupal\contextual_reports\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Contextual Report Context plugins.
 */
interface ContextualReportContextInterface extends PluginInspectionInterface {

  // Add get/set methods for your plugin type here.
}
