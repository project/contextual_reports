<?php

namespace Drupal\contextual_reports\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Contextual report data plugins.
 */
interface ContextualReportDataInterface extends PluginInspectionInterface {

  // Add get/set methods for your plugin type here.
}
