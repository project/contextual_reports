<?php

namespace Drupal\contextual_reports\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Contextual Report plugins.
 */
abstract class ContextualReportBase extends PluginBase implements ContextualReportInterface {

  /**
   * Generate report in a render array format.
   *
   * @param mixed $data
   *   Array containing report data.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Render array.
   */
  public static function generateReport($data, array $params = []) {
    return [];
  }

}
