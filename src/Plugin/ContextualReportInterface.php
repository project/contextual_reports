<?php

namespace Drupal\contextual_reports\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Contextual Report plugins.
 */
interface ContextualReportInterface extends PluginInspectionInterface {

  // Add get/set methods for your plugin type here.
}
