<?php

namespace Drupal\contextual_reports\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Contextual report data plugin manager.
 */
class ContextualReportDataManager extends DefaultPluginManager {

  /**
   * Constructs a new ContextualReportDataManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ContextualReportData', $namespaces, $module_handler, 'Drupal\contextual_reports\Plugin\ContextualReportDataInterface', 'Drupal\contextual_reports\Annotation\ContextualReportData');

    $this->alterInfo('contextual_reports_contextual_report_data_info');
    $this->setCacheBackend($cache_backend, 'contextual_reports_contextual_report_data_plugins');
  }

}
