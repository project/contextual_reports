<?php

namespace Drupal\contextual_reports\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Contextual report data plugins.
 */
abstract class ContextualReportDataBase extends PluginBase implements ContextualReportDataInterface {

  /**
   * Generate report data for ::generateReport().
   *
   * @param array $entity_ids
   *   Array of entity IDs, keyed by entity type or __custom.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array containing report data, for use by ::generateReport().
   */
  public static function generateReportData(array $entity_ids, array $params = []) {
    return [];
  }

}
