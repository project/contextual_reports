<?php

namespace Drupal\contextual_reports\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Contextual Report Context plugins.
 */
abstract class ContextualReportContextBase extends PluginBase implements ContextualReportContextInterface {

  /**
   * Get a list of entity IDs for use by ::generateReportData().
   *
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array of entity IDs, keyed by entity type or __custom.
   */
  public static function getEntities(array $params = []) {
    return [];
  }

}
