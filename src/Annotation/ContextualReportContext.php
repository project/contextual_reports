<?php

namespace Drupal\contextual_reports\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Contextual Report Context item annotation object.
 *
 * @see \Drupal\contextual_reports\Plugin\ContextualReportContextManager
 * @see plugin_api
 *
 * @Annotation
 */
class ContextualReportContext extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
