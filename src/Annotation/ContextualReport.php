<?php

namespace Drupal\contextual_reports\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Contextual Report item annotation object.
 *
 * @see \Drupal\contextual_reports\Plugin\ContextualReportManager
 * @see plugin_api
 *
 * @Annotation
 */
class ContextualReport extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
