<?php

namespace Drupal\contextual_reports;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Contextual Report Entity entities.
 *
 * @ingroup contextual_reports
 */
class ContextualReportEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Contextual Report Entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\contextual_reports\Entity\ContextualReportEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.contextual_report_entity.edit_form',
      ['contextual_report_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
