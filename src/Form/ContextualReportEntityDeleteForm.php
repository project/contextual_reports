<?php

namespace Drupal\contextual_reports\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Contextual Report Entity entities.
 *
 * @ingroup contextual_reports
 */
class ContextualReportEntityDeleteForm extends ContentEntityDeleteForm {


}
