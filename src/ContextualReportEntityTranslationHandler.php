<?php

namespace Drupal\contextual_reports;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for contextual_report_entity.
 */
class ContextualReportEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
