<?php

namespace Drupal\contextual_reports;

use Drupal\contextual_reports\Entity\ContextualReportEntity;

/**
 * Class ContextualReportsUtilities.
 *
 * @package Drupal\contextual_reports
 */
class ContextualReportsUtilities {

  /*
   ****************************************************************************
   * Report Entities
   *
   * Report entities are snapshots of report data, so that reports may be
   * archived and re-loaded at a future date. This allows for easy retrieval
   * and comparison of archived report data.
   ****************************************************************************
   */

  /**
   * Creates a ContextualReportEntity.
   *
   * @param string $context_plugin
   *   String ID of the context plugin.
   * @param string $data_plugin
   *   String ID of the data plugin.
   * @param string $report_plugin
   *   String ID of the report plugin.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   * @param array $entity_data
   *   Extra data to pass to ContextualReportEntity::create().
   *
   *   Use this if you created a different bundle, which has additional fields.
   *
   * @return int
   *   1 if creation is successful, 0 if not.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   This is thrown if creating/saving the entity fails.
   */
  public static function createReportEntity($context_plugin, $data_plugin, $report_plugin, array $params = [], array $entity_data = []) {
    $data = ContextualReportsUtilities::generateReportData($context_plugin, $params);
    $entity_data['context_plugin'] = $context_plugin;
    $entity_data['data_plugin'] = $data_plugin;
    $entity_data['report_plugin'] = $report_plugin;
    $entity_data['params'] = $params;
    $entity_data['data'] = $data;
    $entity_data['type'] = isset($entity_data['type']) ? $entity_data['type'] : 'default';
    $report = ContextualReportEntity::create($entity_data)->save();
    return $report;
  }

  /*
   ****************************************************************************
   * Reports
   *
   * Note that while ::generateReport() and ::generateReportData() are
   * allowed to be completely free-form in what they return, ::getEntities()
   * has a standard output, to help with interoperability.
   *
   * ::getEntities() must return an associative array of entity IDs in the
   * following format:
   *   - A key of an entity type, with the value being a list of IDs
   *   - A key called "__custom" , which may be free-form.
   ****************************************************************************
   */

  /**
   * Get a list of entity IDs for use by ::generateReportData().
   *
   * @param string $context_plugin
   *   String ID of the context plugin.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array of entity IDs, keyed by entity type or __custom.
   */
  public static function getEntities($context_plugin, array $params = []) {
    $plugin_manager = \Drupal::service('plugin.manager.contextual_report_context');
    $plugin_definitions = $plugin_manager->getDefinitions();
    if (isset($plugin_definitions[$context_plugin]['class'])) {
      return $plugin_definitions[$context_plugin]['class']::getEntities($params);
    }
    // We have an invalid context plugin.
    return [];
  }

  /**
   * Generate report data for ::generateReport().
   *
   * @param string $context_plugin
   *   String ID of the context plugin.
   * @param string $data_plugin
   *   String ID of the data plugin.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array containing report data, for use by ::generateReport().
   */
  public static function generateReportData($context_plugin, $data_plugin, array $params = []) {
    $entity_ids = ContextualReportsUtilities::getEntities($context_plugin, $params);
    $plugin_manager = \Drupal::service('plugin.manager.contextual_report_data');
    $plugin_definitions = $plugin_manager->getDefinitions();
    if (isset($plugin_definitions[$data_plugin]['class'])) {
      return $plugin_definitions[$data_plugin]['class']::generateReportData($entity_ids, $params);
    }
    // We have an invalid data plugin.
    return [];
  }

  /**
   * Generate report in a render array format.
   *
   * @param string $context_plugin
   *   String ID of the context plugin.
   * @param string $data_plugin
   *   String ID of the data plugin.
   * @param string $report_plugin
   *   String ID of the report plugin.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Render array.
   */
  public static function generateReport($context_plugin, $data_plugin, $report_plugin, array $params = []) {
    $report_data = ContextualReportsUtilities::generateReportData($context_plugin, $data_plugin, $params);
    if (!empty($report_data)) {
      $plugin_manager = \Drupal::service('plugin.manager.contextual_report');
      $plugin_definitions = $plugin_manager->getDefinitions();
      if (isset($plugin_definitions[$report_plugin]['class'])) {
        return $plugin_definitions[$report_plugin]['class']::generateReport($report_data, $params);
      }
    }
    // We have no data, or we have an invalid report plugin.
    return [];
  }

}
