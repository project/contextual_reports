<?php

namespace Drupal\contextual_reports\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Contextual Report Entity type entity.
 *
 * @ConfigEntityType(
 *   id = "contextual_report_entity_type",
 *   label = @Translation("Contextual Report Entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contextual_reports\ContextualReportEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\contextual_reports\Form\ContextualReportEntityTypeForm",
 *       "edit" = "Drupal\contextual_reports\Form\ContextualReportEntityTypeForm",
 *       "delete" = "Drupal\contextual_reports\Form\ContextualReportEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\contextual_reports\ContextualReportEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "contextual_report_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "contextual_report_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/contextual_report_entity_type/{contextual_report_entity_type}",
 *     "add-form" = "/admin/structure/contextual_report_entity_type/add",
 *     "edit-form" = "/admin/structure/contextual_report_entity_type/{contextual_report_entity_type}/edit",
 *     "delete-form" = "/admin/structure/contextual_report_entity_type/{contextual_report_entity_type}/delete",
 *     "collection" = "/admin/structure/contextual_report_entity_type"
 *   }
 * )
 */
class ContextualReportEntityType extends ConfigEntityBundleBase implements ContextualReportEntityTypeInterface {

  /**
   * The Contextual Report Entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Contextual Report Entity type label.
   *
   * @var string
   */
  protected $label;

}
