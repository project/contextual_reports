<?php

namespace Drupal\contextual_reports\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Contextual Report Entity entities.
 *
 * @ingroup contextual_reports
 */
interface ContextualReportEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Contextual Report Entity name.
   *
   * @return string
   *   Name of the Contextual Report Entity.
   */
  public function getName();

  /**
   * Sets the Contextual Report Entity name.
   *
   * @param string $name
   *   The Contextual Report Entity name.
   *
   * @return \Drupal\contextual_reports\Entity\ContextualReportEntityInterface
   *   The called Contextual Report Entity entity.
   */
  public function setName($name);

  /**
   * Gets the Contextual Report Entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Contextual Report Entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Contextual Report Entity creation timestamp.
   *
   * @param int $timestamp
   *   The Contextual Report Entity creation timestamp.
   *
   * @return \Drupal\contextual_reports\Entity\ContextualReportEntityInterface
   *   The called Contextual Report Entity entity.
   */
  public function setCreatedTime($timestamp);

}
