<?php

namespace Drupal\contextual_reports\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Contextual Report Entity entity.
 *
 * @ingroup contextual_reports
 *
 * @ContentEntityType(
 *   id = "contextual_report_entity",
 *   label = @Translation("Contextual Report Entity"),
 *   bundle_label = @Translation("Contextual Report Entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contextual_reports\ContextualReportEntityListBuilder",
 *     "views_data" = "Drupal\contextual_reports\Entity\ContextualReportEntityViewsData",
 *     "translation" = "Drupal\contextual_reports\ContextualReportEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\contextual_reports\Form\ContextualReportEntityForm",
 *       "add" = "Drupal\contextual_reports\Form\ContextualReportEntityForm",
 *       "edit" = "Drupal\contextual_reports\Form\ContextualReportEntityForm",
 *       "delete" = "Drupal\contextual_reports\Form\ContextualReportEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\contextual_reports\ContextualReportEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\contextual_reports\ContextualReportEntityAccessControlHandler",
 *   },
 *   base_table = "contextual_report_entity",
 *   data_table = "contextual_report_entity_field_data",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer contextual report entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/contextual_report_entity/{contextual_report_entity}",
 *     "add-page" = "/admin/structure/contextual_report_entity/add",
 *     "add-form" = "/admin/structure/contextual_report_entity/add/{contextual_report_entity_type}",
 *     "edit-form" = "/admin/structure/contextual_report_entity/{contextual_report_entity}/edit",
 *     "delete-form" = "/admin/structure/contextual_report_entity/{contextual_report_entity}/delete",
 *     "collection" = "/admin/structure/contextual_report_entity",
 *   },
 *   bundle_entity_type = "contextual_report_entity_type",
 *   field_ui_base_route = "entity.contextual_report_entity_type.edit_form"
 * )
 */
class ContextualReportEntity extends ContentEntityBase implements ContextualReportEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Contextual Report Entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Contextual Report Entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    /*
     * Contextual Report Fields
     */

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('The generated report data.'));

    $fields['params'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Parameters'))
      ->setDescription(t('The parameters used to build the report data.'));

    $fields['report_plugin'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Report'))
      ->setDescription(t('The string ID of the @ContextualReport plugin.'));

    $fields['data_plugin'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Data'))
      ->setDescription(t('The string ID of the @ContextualReportData plugin.'));

    $fields['context_plugin'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Context'))
      ->setDescription(t('The string ID of the @ContextualReportContext plugin.'));

    return $fields;
  }

}
