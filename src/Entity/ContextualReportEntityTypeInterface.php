<?php

namespace Drupal\contextual_reports\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Contextual Report Entity type entities.
 */
interface ContextualReportEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
