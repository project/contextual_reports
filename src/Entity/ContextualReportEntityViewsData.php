<?php

namespace Drupal\contextual_reports\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Contextual Report Entity entities.
 */
class ContextualReportEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
