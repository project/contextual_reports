CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Notices
 * Faqs
 * Maintainers


INTRODUCTION
------------

This is a simple Annotation Plugin API that generates reports based on 
contexts. This module is primarily an API module meant for use cases where
someone is making similar reports in different contexts, in a way that
requires fast and systematic construction of all reports. 

REQUIREMENTS
------------

This module does not require any other modules.


INSTALLATION
------------

Install the module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.


CONFIGURATION
-------------

This module does not require configuration.

FAQs
----

**How do I create a contextual report?**

A contextual report requires the following:
- Context: Specifies which entities to fetch for the report
- Data: Converts entities into report data
- Report: Generates a render array from entities provided by a context
  - More on render arrays: 
  https://www.drupal.org/docs/8/api/render-api/render-arrays
- Optional parameters for the context, data, and report

The identifiers of the context, data, and report, along with an associative
array of parameters, may be provided to the generateReport() function in the
\Drupal\contextual_reports\ContextualReportsUtilities class. This will
return a render array which may be displayed on a page, block, form, or
anywhere else it is needed.


**What if I just want data?**

Instead of ContextualReportsUtilities::generateReport(), use 
ContextualReportsUtilities::generateReportData() instead. The structure of
the data will depend on how the report plugin class generates the data.


**What plugins do I need to create?**

*@ContextualReportContext*

This will fetch entities. Put these in 
src/Plugin/ContextualReportContext. The following static methods must
be available:

  - ::getEntities()
    - Returns an associative array keyed to the entity type, with a list
    of entity IDs. A '__custom' key may also be used if you require a
    customized data structure for ::generateReportData()

*@ContextualReportData*

This will take a list of entities, and generate an associative array of report 
data. Put these in src/Plugin/ContextualReportData. The following static 
methods must be available:

  - ::generateReportData()
    - Returns an array in the format expected by 
      @ContextualReport::generateReport() 

*@ContextualReport*

This will generate the report. Put these in src/Plugin/ContextualReport.

  - ::generateReport()
    - Returns a render array, which produces the report
  - ::renderParameters()
    - Returns a render array, to display the parameters
    - This function is optional, but recommended for use with
    report entities. If not set, the parameters will be passed to
    the print_r() function if shown.

**Do you have a working example?**

An example has been provided in the contextual_reports_example module, under
modules/contextual_reports_example.
    

**How do I take a snapshot of a report?**

The ContextualReportsUtilities class has a function, 
createReportEntity(), which lets you record a report's data into a
ContextualReportEntity, along with the data needed to generate the report
(i.e. the context and plugin). This modules comes with a 'default' bundle,
and you may create additional bundles (report entity types) at 
/admin/structure/contextual_report_entity_type . The createReportEntity()
function will allow you to pass additional data to the entity's create()
function, if you need to create a new report entity type that requires 
additional data.
 

MAINTAINERS
-----------

The original module was built by Michael Nolan (laboratory.mike) to cover needs
within the Niobi Research Center (https://www.drupal.org/project/niobi)
project.

 * Michael Nolan: https://www.drupal.org/u/laboratorymike
