<?php

namespace Drupal\contextual_reports_example\Controller;

use Drupal\contextual_reports\ContextualReportsUtilities;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class ContextualReportsExampleController.
 *
 * @package Drupal\contextual_reports_example\Controller
 */
class ContextualReportsExampleController extends ControllerBase {

  /**
   * Generates a table with the names of the ten most recent users.
   *
   * @return array
   *   Render array
   */
  public function examplePage() {
    return ContextualReportsUtilities::generateReport('contextual_reports_example', 'contextual_reports_example', []);
  }

}
