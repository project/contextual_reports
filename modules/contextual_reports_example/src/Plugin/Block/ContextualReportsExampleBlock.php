<?php

namespace Drupal\contextual_reports_example\Plugin\Block;

use Drupal\contextual_reports\ContextualReportsUtilities;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ContextualReportsExampleBlock' block.
 *
 * @Block(
 *  id = "contextual_reports_example_block",
 *  admin_label = @Translation("Contexual reports example block"),
 * )
 */
class ContextualReportsExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $params = ['title' => $this->t('Ten Most Recent Users')];
    $context = 'contextual_reports_example';
    $data = 'contextual_reports_example';
    $report = 'contextual_reports_example';
    $build = ContextualReportsUtilities::generateReport($context, $data, $report, $params);

    return $build;
  }

}
