<?php

namespace Drupal\contextual_reports_example\Plugin\ContextualReport;

use Drupal\contextual_reports\Plugin\ContextualReportBase;

/**
 * Creates a table containing a list of user display names.
 *
 * @ContextualReport (
 *   id = "contextual_reports_example",
 *   label = "User Names",
 *   description = "Simple table of user names",
 *   category = "contextual_reports_example"
 * )
 */
class UserDisplayNames extends ContextualReportBase {

  /**
   * Creates a table of display names.
   *
   * @param mixed $data
   *   An array of display names.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Render array.
   */
  public static function generateReport($data, array $params = []) {
    $page = [];
    $page['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => isset($params['title']) ? $params['title'] : t('List of Users'),
    ];
    $page['table'] = [
      '#type' => 'table',
      '#theme' => 'table',
      '#header' => [
        t('User'),
      ],
    ];
    $page['table']['#rows'] = [];

    foreach ($data as $d) {
      $page['table']['#rows'][] = [
        'user' => $d,
      ];
    }
    $page['table'];

    return $page;
  }

}
