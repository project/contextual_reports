<?php

namespace Drupal\contextual_reports_example\Plugin\ContextualReportData;

use Drupal\contextual_reports\Plugin\ContextualReportDataBase;
use Drupal\user\Entity\User;

/**
 * Class to get user names from a list of users.
 *
 * @ContextualReportData (
 *   id = "contextual_reports_example",
 *   label = "Ten Newest Users",
 *   description = "Simple array of user names",
 *   category = "contextual_reports_example"
 * )
 */
class UserNames extends ContextualReportDataBase {

  /**
   * Generates a list of user names.
   *
   * @param array $entity_ids
   *   List of user entity IDs.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array of data to be used by a report function.
   */
  public static function generateReportData(array $entity_ids, array $params = []) {
    $data = [];
    $entities = User::loadMultiple($entity_ids['user']);
    foreach ($entities as $uid => $user) {
      /* @var $user \Drupal\user\Entity\User */
      $data[$uid] = $user->getDisplayName();
    }
    return $data;
  }

}
