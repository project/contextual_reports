<?php

namespace Drupal\contextual_reports_example\Plugin\ContextualReportContext;

use Drupal\contextual_reports\Plugin\ContextualReportContextBase;

/**
 * Gets the ten most recent users by create date.
 *
 * @ContextualReportContext (
 *   id = "contextual_reports_example",
 *   label = "Ten Newest Users",
 *   description = "Simple table of user names",
 *   category = "contextual_reports_example"
 * )
 */
class TenNewestUsers extends ContextualReportContextBase {

  /**
   * Gets the ten most recent users by create date.
   *
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   List of user entity IDs.
   */
  public static function getEntities(array $params = []) {
    $query = \Drupal::entityQuery('user');
    $query->sort('created', 'DESC');
    $query->range(0, 10);
    $result = $query->execute();
    return ['user' => $result];
  }

}
