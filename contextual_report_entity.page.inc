<?php

/**
 * @file
 * Contains contextual_report_entity.page.inc.
 *
 * Page callback for Contextual Report Entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Contextual Report Entity templates.
 *
 * Default template: contextual_report_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_contextual_report_entity(array &$variables) {

  /*
   * TODO: Determine if we need the following.
   *
   * Fetch ContextualReportEntity Entity Object.
   * $contextual_report_entity =
   * $variables['elements']['#contextual_report_entity'];
   */

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

}
